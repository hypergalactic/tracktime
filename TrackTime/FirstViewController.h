//
//  FirstViewController.h
//  TrackTime
//
//  Created by Robert Wasmann on 2015-04-03.
//  Copyright (c) 2015 Big Muddy Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SecondViewController.h"

@protocol SimDelegate <NSObject>
@optional
- (void)didFinishCalculating;
- (void)performSelectorOnMainThread:(SEL)aSelector withObject:(id)arg waitUntilDone:(BOOL)wait;
@end

@interface FirstViewController : UIViewController <SecondViewControllerDelegate, SimDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet id <SimDelegate> delegate;
@property NSOperationQueue *opQueue;
@property (nonatomic)  UITextField *currentTextfieldId;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (nonatomic) UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UITextField *trackName_textField;
@property (weak, nonatomic) IBOutlet UITextField *carName_textField;
@property (weak, nonatomic) IBOutlet UITextField *trackTime_textField;
@property (weak, nonatomic) IBOutlet UITextField *trackLength_textField;
@property (weak, nonatomic) IBOutlet UITextField *topSpeed_textField;
@property (weak, nonatomic) IBOutlet UIButton *simButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

- (IBAction)keyboardCloseClicked:(id)sender;
- (void)didFinishCalculating;
- (void)didFinishSharing;
- (IBAction)share:(id)sender;

@end


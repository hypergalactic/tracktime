//
//  FirstViewController.m
//  TrackTime
//
//  Created by Robert Wasmann on 2015-04-03.
//  Copyright (c) 2015 Big Muddy Games. All rights reserved.
//

#import "FirstViewController.h"
#import "MathFunctions.h"
#import "CGPointExtension.h"
#import "Macros.h"
#import "Appirater.h"

#define GRAVITY 9.80665f // m/s^2
#define FEET_PER_METER 3.28084f
#define METERS_PER_MILE 1609.34f
#define MILES_PER_KM 0.62137f
#define METERS_SEC_PER_MPH 0.44704f
#define SIXTY_MPH_TO_METERS_SEC 26.8224f

#define CAR_WIDTH 1.83 // meters
#define TRACK_WIDTH 12.8 // FIA spec in meters
#define MOSPORT_TURN1_RADIUS 128.0 // meters
#define MOSPORT_TURN1_ANGLE 1.81048 // Radians
#define STD_LATERAL_ACCEL 0.90

#define kFieldSeperation 20  // Default distance between UI elements in *most* cases. Used for aligning the scroll view with keyboard.


@interface FirstViewController ()


@end

@implementation FirstViewController
{
    NSMutableArray  *_trackSegments;
    NSArray         *_tracks;
    CGFloat         _trackTime;
    CGFloat         _trackLength;
    CGFloat         _topSpeed;
    NSString        *_trackTimeString;
    NSString        *_trackName;
    NSString        *_carName;
}

- (void)viewDidLoad;
{
    [super viewDidLoad];
    
    //FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    //loginButton.center = self.view.center;
    //[self.view addSubview:loginButton];
    
    self.opQueue = [[NSOperationQueue alloc] init];
    [self.opQueue setMaxConcurrentOperationCount:1];
    
    // Do any additional setup after loading the view, typically from a nib.
    
    SecondViewController *svc = [self.tabBarController.viewControllers objectAtIndex:1];
    
    svc.delegate = self; // important! otherwise delegation will not work!
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    id carName = [defaults objectForKey:@"defaultNameKEY"];
    if (carName == nil) {
        self.carName_textField.text = [NSString stringWithFormat:@"2007 Porsche 911 Turbo"];
    } else {
        self.carName_textField.text = carName;
    }
    
    _tracks = @[@"Laguna Seca, USA", @"Monte Carlo, Monaco",
                @"Monza, Italy", @"Mosport, Canada", @"Suzuka, Japan"];
    
    // Connect track picker data
    
    _pickerView = [UIPickerView new];

    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    
    self.trackName_textField.text = [NSString stringWithFormat:@"Laguna Seca, USA"];
    //self.trackWidth_textField.text = [NSString stringWithFormat:@"%f", TRACK_WIDTH];
    
    [self loadTrackSegments:self.trackName_textField.text];
    
}

- (void)loadTrackSegments:(NSString *)trackName;
{
    if (!([trackName rangeOfString:@"Laguna"].location == NSNotFound)) {
        // Must start with a corner for simulation to work.
        // First number is either 0 or 1, 0 is a corner, 1 is a straight.
        // Second number is the curve radius in ft, or straight length in ft.
        // Third number is the curve angle in radians
        
        // Laguna Seca, Monterey California
        // Turn 1
        NSArray *segment1 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:516.76], [NSNumber numberWithFloat:0.35343], nil];
        NSArray *segment2 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:580.97], nil];
        // Turn 2
        NSArray *segment3 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:127.0], [NSNumber numberWithFloat:3.44740], nil];
        NSArray *segment4 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:637.98], nil];
        // Turn 3
        NSArray *segment5 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:145.0], [NSNumber numberWithFloat:1.62159], nil];
        NSArray *segment6 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:628.67], nil];
        // Turn 4
        NSArray *segment7 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:200.0], [NSNumber numberWithFloat:1.25875], nil];
        NSArray *segment8 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:1324.86], nil];
        // Turn 5
        NSArray *segment9 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:170.0], [NSNumber numberWithFloat:1.49847], nil];
        NSArray *segment10 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:1009.53], nil];
        // Turn 6
        NSArray *segment11 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:80.0], [NSNumber numberWithFloat:0.90488], nil];
        NSArray *segment12 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:224.92], nil];
        NSArray *segment13 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:500.0], [NSNumber numberWithFloat:0.32542], nil];
        NSArray *segment14 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:876.94], nil];
        // Turn 7
        NSArray *segment15 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:450.0], [NSNumber numberWithFloat:0.42787], nil];
        NSArray *segment16 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:127.74], nil];
        // Turn 8
        NSArray *segment17 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:75.0], [NSNumber numberWithFloat:1.97200], nil];
        NSArray *segment18 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:90.0], [NSNumber numberWithFloat:1.44311], nil];
        NSArray *segment19 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:415.97], nil];
        // Turn 9
        NSArray *segment20 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:265.0], [NSNumber numberWithFloat:1.81981], nil];
        NSArray *segment21 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:526.91], nil];
        // Turn 10
        NSArray *segment22 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:150.0], [NSNumber numberWithFloat:1.40833], nil];
        NSArray *segment23 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:798.74], nil];
        // Turn 11
        NSArray *segment24 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:45.0], [NSNumber numberWithFloat:2.25422], nil];
        // Home Straight
        NSArray *segment25 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:1621.65], nil];
        //NSArray *segment25 = @[@"1", @"1621.65"];

        _trackSegments = [NSMutableArray arrayWithObjects:segment1, segment2, segment3, segment4,
                          segment5, segment6, segment7, segment8, segment9,
                          segment10, segment11, segment12, segment13, segment14,
                          segment15, segment16, segment17, segment18, segment19,
                          segment20, segment21, segment22, segment23, segment24,
                          segment25, nil];

    } else if (!([trackName rangeOfString:@"Mosport"].location == NSNotFound)) {
        
        // MOSPORT BOWMANVILLE, ONTARIO
        // Turn 1
        NSArray *segment1 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:420.0], [NSNumber numberWithFloat:1.81048], nil];
        NSArray *segment2 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:834.0], nil];
        // Turn 2
        NSArray *segment3 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:500.0], [NSNumber numberWithFloat:2.00543], nil];
        NSArray *segment4 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:559.0], nil];
        // Turn 3
        NSArray *segment5 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:300.0], [NSNumber numberWithFloat:1.66446], nil];
        NSArray *segment6 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:577.208], [NSNumber numberWithFloat:0.739438], nil];
        NSArray *segment7 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:759.0], nil];
        // Turn 4
        NSArray *segment8 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:400.0], [NSNumber numberWithFloat:1.12409], nil];
        NSArray *segment9 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:139.0], nil];
        // Turn 5A
        NSArray *segment10 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:160.0], [NSNumber numberWithFloat:1.73660], nil];
        NSArray *segment11 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:120.0], nil];
        // Moss Corner
        // Turn 5B
        NSArray *segment12 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:34.0], [NSNumber numberWithFloat:1.57080], nil];
        NSArray *segment13 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:150.0], nil];
        // Turn 5C
        NSArray *segment14 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:800.0], [NSNumber numberWithFloat:0.645045], nil];
        NSArray *segment15 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:759.0], nil];
        // Turn 6
        NSArray *segment16 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:2000.0], [NSNumber numberWithFloat:0.157080], nil];
        NSArray *segment17 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:792.0], nil];
        // Turn 7
        NSArray *segment18 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:2000], [NSNumber numberWithFloat:0.281143], nil];
        NSArray *segment19 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:769.0], nil];
        // Turn 8
        NSArray *segment20 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:500.0], [NSNumber numberWithFloat:1.71314], nil];
        NSArray *segment21 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:143.0], nil];
        // Turn 9
        NSArray *segment22 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:340.0], [NSNumber numberWithFloat:1.23482], nil];
        NSArray *segment23 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:111.0], nil];
        // Turn 10
        NSArray *segment24 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:200.0], [NSNumber numberWithFloat:1.65370], nil];
        // Home Straight
        NSArray *segment25 = [NSArray arrayWithObjects:[NSNumber numberWithFloat:1], [NSNumber numberWithFloat:921.0], nil];
        
        _trackSegments = [NSMutableArray arrayWithObjects:segment1, segment2, segment3, segment4,
                          segment5, segment6, segment7, segment8, segment9,
                          segment10, segment11, segment12, segment13, segment14,
                          segment15, segment16, segment17, segment18, segment19,
                          segment20, segment21, segment22, segment23, segment24,
                          segment25, nil];
        
    } else if (!([trackName rangeOfString:@"Monaco"].location == NSNotFound)) {
        
        // Monte Carlo, Monaco
        // Turn 1
        NSArray *segment1 = @[@"0", @"178.1", @"1.571"];
        NSArray *segment2 = @[@"1", @"1488.0"];
        // Turn 3
        NSArray *segment3 = @[@"0", @"225.3", @"1.920"];
        NSArray *segment4 = @[@"1", @"100.8"];
        // Turn 4
        NSArray *segment5 = @[@"0", @"145.3", @"1.222"];
        NSArray *segment6 = @[@"1", @"593.2"];
        // Turn 5
        NSArray *segment7 = @[@"0", @"73.05", @"2.269"];
        NSArray *segment8 = @[@"1", @"279.4"];
        // Turn 6
        NSArray *segment9 = @[@"0", @"40.42", @"3.142"];
        NSArray *segment10 = @[@"1", @"113.5"];
        // Turn 7
        NSArray *segment11 = @[@"0", @"68.24", @"1.920"];
        NSArray *segment12 = @[@"1", @"160.0"];
        // Turn 8
        NSArray *segment13 = @[@"0", @"77.09", @"1.920"];
        NSArray *segment14 = @[@"1", @"300.0"];
        // Turn 9
        NSArray *segment15 = @[@"0", @"1254.0", @"1.047"];
        NSArray *segment16 = @[@"1", @"413.4"];
        // Turn 10 Chicane
        NSArray *segment17 = @[@"0", @"41.73", @"1.571"];
        NSArray *segment18 = @[@"0", @"41.73", @"1.571"];
        // Turn 11 (Mostly straight)
        NSArray *segment19 = @[@"1", @"732.8"];
        // Turn 12
        NSArray *segment20 = @[@"0", @"246.7", @"1.047"];
        NSArray *segment21 = @[@"1", @"377.7"];
        // Turn 13
        NSArray *segment22 = @[@"0", @"123.2", @"0.6981"];
        // Turn 14
        NSArray *segment23 = @[@"0", @"123.2", @"0.6981"];
        NSArray *segment24 = @[@"1", @"276.3"];
        // Turn 15
        NSArray *segment25 = @[@"0", @"78.99", @"1.396"];
        // Turn 16
        NSArray *segment26 = @[@"0", @"78.99", @"1.396"];
        NSArray *segment27 = @[@"1", @"393.3"];
        // Turn 17
        NSArray *segment28 = @[@"0", @"78.97", @"3.142"];
        NSArray *segment29 = @[@"1", @"145.0"];
        // Turn 18
        NSArray *segment30 = @[@"0", @"82.57", @"1.222"];
        // Final Straight
        NSArray *segment31 = @[@"1", @"1573.0"];
        
        _trackSegments = [NSMutableArray arrayWithObjects:segment1, segment2, segment3, segment4,
                          segment5, segment6, segment7, segment8, segment9,
                          segment10, segment11, segment12, segment13, segment14,
                          segment15, segment16, segment17, segment18, segment19,
                          segment20, segment21, segment22, segment23, segment24,
                          segment25, segment26, segment27, segment28, segment29,
                          segment30, segment31, nil];
        
        
    } else if (!([trackName rangeOfString:@"Monza"].location == NSNotFound)) {
        
        // Monza, Italy
        // Turn 1
        NSArray *segment1 = @[@"0", @"79.0", @"1.571"];
        // Turn 2
        NSArray *segment2 = @[@"0", @"79.2", @"1.745"];
        NSArray *segment3 = @[@"1", @"1008.0"];
        // Turn 3
        NSArray *segment4 = @[@"0", @"988.8", @"1.222"];
        NSArray *segment5 = @[@"1", @"1348.0"];
        // Turn 4
        NSArray *segment6 = @[@"0", @"205.0", @"1.222"];
        // Turn 5
        NSArray *segment7 = @[@"0", @"85.25", @"0.8727"];
        NSArray *segment8 = @[@"1", @"902.2"];
        // Turn 6
        NSArray *segment9 = @[@"0", @"264.7", @"1.745"];
        NSArray *segment10 = @[@"1", @"751.0"];
        // Turn 7
        NSArray *segment11 = @[@"0", @"171.9", @"1.047"];
        NSArray *segment12 = @[@"1", @"3239.0"];
        // Turn 8
        NSArray *segment13 = @[@"0", @"267.6", @"0.8727"];
        // Turn 9
        NSArray *segment14 = @[@"0", @"338.0", @"1.222"];
        // Turn 10
        NSArray *segment15 = @[@"0", @"336.0", @"0.6981"];
        NSArray *segment16 = @[@"1", @"3062.0"];
        // Turn 11
        NSArray *segment17 = @[@"0", @"269.4", @"1.396"];
        NSArray *segment18 = @[@"0", @"684.3", @"1.222"];
        // Final Straight
        NSArray *segment19 = @[@"1", @"3978.0"];
        
        _trackSegments = [NSMutableArray arrayWithObjects:segment1, segment2, segment3, segment4,
                          segment5, segment6, segment7, segment8, segment9,
                          segment10, segment11, segment12, segment13, segment14,
                          segment15, segment16, segment17, segment18, segment19, nil];
        
    } else if (!([trackName rangeOfString:@"Suzuka"].location == NSNotFound)) {
        
        // Suzuka, Japan
        // Turn 1
        NSArray *segment1 = @[@"0", @"467.1", @"0.8727"];
        // Turn 2
        NSArray *segment2 = @[@"0", @"237.2", @"0.2.269"];
        NSArray *segment3 = @[@"1", @"446.4"];
        // Turn 3
        NSArray *segment4 = @[@"0", @"290.2", @"1.047"];
        // Turn 4
        NSArray *segment5 = @[@"0", @"284.6", @"1.571"];
        // Turn 5
        NSArray *segment6 = @[@"0", @"355.6", @"1.571"];
        // Turn 6
        NSArray *segment7 = @[@"0", @"281.6", @"1.920"];
        NSArray *segment8 = @[@"1", @"92.0"];
        // Turn 7
        NSArray *segment9 = @[@"0", @"354.0", @"1.571"];
        NSArray *segment10 = @[@"0", @"711.1", @"1.222"];
        NSArray *segment11 = @[@"1", @"482.7"];
        // Turn 8
        NSArray *segment12 = @[@"0", @"360.1", @"0.6981"];
        NSArray *segment13 = @[@"1", @"233.4"];
        // Turn 9
        NSArray *segment14 = @[@"0", @"181.8", @"1.571"];
        NSArray *segment15 = @[@"1", @"630.2"];
        // Turn 10
        NSArray *segment16 = @[@"0", @"713.4", @"0.6981"];
        NSArray *segment17 = @[@"1", @"98.0"];
        // Turn 11 (Hairpin)
        NSArray *segment18 = @[@"0", @"90.24", @"3.142"];
        NSArray *segment19 = @[@"1", @"364.3"];
        // Turn 12
        NSArray *segment20 = @[@"0", @"738.5", @"1.222"];
        NSArray *segment21 = @[@"1", @"264.6"];
        NSArray *segment22 = @[@"0", @"947.0", @"0.6981"];
        NSArray *segment23 = @[@"1", @"307.4"];
        // Turn 13
        NSArray *segment24 = @[@"0", @"359.4", @"1.571"];
        // Turn 14
        NSArray *segment25 = @[@"0", @"298.8", @"1.920"];
        NSArray *segment26 = @[@"1", @"2734.0"];
        // Turn 15
        NSArray *segment27 = @[@"0", @"450.2", @"0.8727"];
        NSArray *segment28 = @[@"1", @"1005"];
        // Turn 16 (Chicane)
        NSArray *segment29 = @[@"0", @"154.5", @"1.396"];
        // Turn 17
        NSArray *segment30 = @[@"0", @"154.5", @"1.396"];
        // Turn 18
        NSArray *segment31 = @[@"0", @"457.3", @"1.396"];
        // Final Straight
        NSArray *segment32 = @[@"1", @"2543.0"];
        
        _trackSegments = [NSMutableArray arrayWithObjects:segment1, segment2, segment3, segment4,
                          segment5, segment6, segment7, segment8, segment9,
                          segment10, segment11, segment12, segment13, segment14,
                          segment15, segment16, segment17, segment18, segment19,
                          segment20, segment21, segment22, segment23, segment24,
                          segment25, segment26, segment27, segment28, segment29,
                          segment30, segment31, segment32, nil];
        
        
    }
}

#pragma mark
#pragma mark textField Delegates

-(bool)textFieldShouldBeginEditing:(UITextField *)textField;
{
    if (textField == self.carName_textField) {
        [self callSpecsTab];
        return NO;
    }
    
    if (textField == self.trackName_textField) {
        
        [self.pickerView setShowsSelectionIndicator:YES];
        self.trackName_textField.inputView = self.pickerView;
        
        
        UIToolbar *myToolbar = [[UIToolbar alloc] initWithFrame:
                                CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height / 17.0f)];
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                    target:self action:@selector(inputAccessoryViewDidFinish)];
        
        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];

        //using default text field delegate method here, here you could call
        //myTextField.resignFirstResponder to dismiss the views
        [myToolbar setItems:[NSArray arrayWithObjects:flex, doneButton, nil] animated:NO];
        self.trackName_textField.inputAccessoryView = myToolbar;
    }
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField;
{
    _currentTextfieldId = textField;
    
}

-(void)textFieldDidChange:(SecondViewController *)controller data:(float)textFieldValue;
{
    
}

-(void)callSpecsTab;
{
    //[self.view endEditing:YES];
    
    [self.tabBarController setSelectedIndex:1];
}

- (IBAction)keyboardCloseClicked:(id)sender;
{
    //--- HIDE KEYBOARD
    UITextField *currentTextfield = (UITextField *)_currentTextfieldId;
    [currentTextfield resignFirstResponder];
    
    //	[self saveSettings];
}

- (void)inputAccessoryViewDidFinish;
{
    [self.currentTextfieldId resignFirstResponder];
}

#pragma mark -
#pragma mark Simulation Methods

- (IBAction)simulate:(id)sender;
{
    [Appirater userDidSignificantEvent:NO];

    //--- HIDE KEYBOARD
    UITextField *currentTextfield = (UITextField *)_currentTextfieldId;
    [currentTextfield resignFirstResponder];
    
    // Show activity indicator
    [self.activity startAnimating];
    self.activity.hidden = NO;
    self.shareButton.hidden = YES;
    self.simButton.userInteractionEnabled = NO;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        self.simButton.highlighted = YES;
    }];
    
    // Clear fields
    self.trackTime_textField.text = [NSString stringWithFormat:@""];
    self.trackLength_textField.text = [NSString stringWithFormat:@""];
    self.topSpeed_textField.text = [NSString stringWithFormat:@""];
    
    //make sure opqueue is initialized
    if(!self.opQueue) {self.opQueue = [[NSOperationQueue alloc] init];}
    
    //create async request
    NSInvocationOperation *request = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(performSim:) object:nil];
    [self.opQueue addOperation:request];
}

-(void)performSim:(id)sender;
{
    CGFloat straightLength;
    CGFloat cornerRadius;
    CGFloat cornerAngle;
    CGFloat cornerTime;
    CGFloat cornerLength;
    CGFloat exitSpeed;
    CGFloat entrySpeed;
    CGFloat accelTime = 0.0f;
    CGFloat decelTime = 0.0f;
    CGFloat cruiseTime = 0.0f;
    CGFloat vMax = 0.0f;
    CGFloat segmentTopSpeed = 0.0f;
    CGFloat avgAccel = 0.0f;
    CGFloat fudgeFactor;
    CGFloat ratio;
    BOOL    simpleCase = NO;
    
    _topSpeed = 0.0f;
    _trackLength = 0.0f;
    _trackTime = 0.0f;
    _trackTimeString = @"";
    _trackName = @"";
    _carName = @"";
    
    //SecondViewController *svc = [self.tabBarController.viewControllers objectAtIndex:1];
    
    
    // Load default car stats, if none available then use default values
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    id carName = [defaults objectForKey:@"defaultNameKEY"];
    
    CGFloat zeroSixty = (double)[defaults doubleForKey:@"defaultZeroSixtyKEY"];
    
    CGFloat quarterMileTime = (double)[defaults doubleForKey:@"defaultQuarterMileTimeKEY"];
    
    CGFloat quarterMileSpeed = (double)[defaults doubleForKey:@"defaultQuarterMileSpeedKEY"];
    
    CGFloat lateralG = (double)[defaults doubleForKey:@"defaultMaxLateralKEY"];
    
    CGFloat braking = (double)[defaults doubleForKey:@"defaultBrakingKEY"];
    
    CGFloat maxSpeed = (double)[defaults doubleForKey:@"defaultTopSpeedKEY"];
    
    _trackName = self.trackName_textField.text;
    _carName = carName;
    
    [defaults synchronize];
    
    if (carName == nil) {
        _carName = @"2007 Porsche 911 Turbo";
        zeroSixty = 3.3f;
        quarterMileTime = 11.6f;
        quarterMileSpeed = 118.3f;
        lateralG = 0.97f;
        braking = 116.0f;
        maxSpeed = 193.0f;
    }
    
    quarterMileSpeed = quarterMileSpeed * METERS_SEC_PER_MPH;  // Convert from mph to m/s
    braking = braking / FEET_PER_METER; // convert from feet to meters
    lateralG = lateralG * GRAVITY;
    maxSpeed = maxSpeed * METERS_SEC_PER_MPH;
    
    CGFloat zeroSixtyAvgAccel = SIXTY_MPH_TO_METERS_SEC / zeroSixty;
    
    CGFloat quarterMileAvgAccel = quarterMileSpeed / quarterMileTime;
    CGFloat avgDecel = SIXTY_MPH_TO_METERS_SEC * SIXTY_MPH_TO_METERS_SEC / ( 2.0f * braking); // using time independant distance equation
    
    //NSLog(@"avgAccel:%f", avgAccel);
    //NSLog(@"avgDecel:%f", avgDecel);
    
    NSUInteger segmentCount = [_trackSegments count];
    NSMutableArray *segmentResults = [NSMutableArray arrayWithCapacity:segmentCount];
    
    
    // Populate the array with nulls so we can insert values at any index later
    for (NSUInteger i = 0; i < segmentCount; i++) {
        [segmentResults addObject:[NSNull null]];
    }
    
    NSLog(@"----------------------");
    NSLog(@"Starting Simulation...");
    NSLog(@" ");
    
    // Calculate the corner times first since the straightaway times depend on the entry and exit speeds
    //NSLog(@"Calculating corners!");
    //NSLog(@" ");
    
    for (int i = 0; i < segmentCount; i++) {
        NSArray *segment = [_trackSegments objectAtIndex:i];
        NSNumber *type = [segment objectAtIndex:0];
        
        if ([type floatValue] == 0) {
            NSNumber *num = [segment objectAtIndex:1];
            cornerRadius = [num floatValue] / FEET_PER_METER;
            
            num = [segment objectAtIndex:2];
            cornerAngle = [num floatValue];
            
            // centripetal acceleration is a = radius * angular speed ^ 2
            // angular speed is just theta / time
            
            cornerTime = cornerAngle / sqrt(lateralG / cornerRadius);
            
            cornerLength = cornerAngle * cornerRadius;
            exitSpeed = cornerLength / cornerTime;
            entrySpeed = cornerLength / cornerTime;
            
            // Check if the corner speed is beyond what the car can handle
            if (exitSpeed > maxSpeed || entrySpeed > maxSpeed) {
                if (exitSpeed > maxSpeed) {
                    exitSpeed = maxSpeed;
                }
                if (entrySpeed > maxSpeed) {
                    entrySpeed = maxSpeed;
                }
                // Calculate a new cornerTime based on the maxSpeed around the length of the curve
                cornerTime = cornerLength / maxSpeed;
            }
            
            cornerLength = cornerLength * FEET_PER_METER;

            //NSLog(@"Segment #:%d", i+1);
            //NSLog(@"Corner Time:%f s", cornerTime);
            //NSLog(@"Corner Length:%f ft", cornerLength);
            //NSLog(@"Entry Speed:%f mph", entrySpeed / METERS_SEC_PER_MPH);
            //NSLog(@"Exit Speed:%f mph", exitSpeed / METERS_SEC_PER_MPH);
            //NSLog(@" ");
            
            _trackTime += cornerTime;
            _trackLength += cornerLength;
            
            NSArray *segmentResult = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:entrySpeed], [NSNumber numberWithFloat:exitSpeed], nil];
            [segmentResults replaceObjectAtIndex:i withObject:segmentResult];
            
            CGFloat segmentTopSpeed = MAX(entrySpeed, exitSpeed);
            segmentTopSpeed = segmentTopSpeed / METERS_SEC_PER_MPH;
            
            if (segmentTopSpeed > _topSpeed) {
                _topSpeed = segmentTopSpeed;
            }

        }
    }
    
    // Now calculate the straightaway times
    
    //NSLog(@"Calculating Straights!");
    //NSLog(@" ");
    
    for (int i = 0; i < segmentCount; i++) {
        
        simpleCase = NO;
        cruiseTime = 0.0f;
        
        NSArray *nextSegmentResult;
        NSArray *segment = [_trackSegments objectAtIndex:i];
        NSNumber *type = [segment objectAtIndex:0];
        
        if ([type floatValue] == 1.0f) {
            
            //NSLog(@"Segment #:%d", i+1);
            
            if ([segmentResults count] == 0) {
                NSLog(@" ");
                NSLog(@"Error: Can not start simulation with a straightaway");
                NSLog(@" ");
                [self didFail];
                return;
            }
            
            NSArray *prevSegmentResult = [segmentResults objectAtIndex:i-1];
            
            // Check to see if we're on the last Straight (and last track segment) in the array
            // If so then use the proper adjoining corner segment
            
            if (i == segmentCount - 1) {
                nextSegmentResult = [segmentResults objectAtIndex:0];
            } else {
                nextSegmentResult = [segmentResults objectAtIndex:i+1];
            }
            
            if ([prevSegmentResult isEqual:[NSNull null]] || [nextSegmentResult isEqual:[NSNull null]]) {
                NSLog(@" ");
                NSLog(@"Error: Can not calculate straights without corner results!");
                NSLog(@" ");
                [self didFail];
                return;
            }
            
            NSNumber *num = [prevSegmentResult objectAtIndex:1];
            exitSpeed = [num floatValue];
            
            num = [nextSegmentResult objectAtIndex:2];
            entrySpeed = [num floatValue];
            
            num = [segment objectAtIndex:1];
            straightLength = [num floatValue] / FEET_PER_METER;
            
            if (exitSpeed < 30.0f * METERS_SEC_PER_MPH && entrySpeed < 65.0f * METERS_SEC_PER_MPH) {
                // Use the 0-60mph time for <30mph to 65mph range
                avgAccel = zeroSixtyAvgAccel;

            } else if (exitSpeed >= 60.0f * METERS_SEC_PER_MPH) {
                // Find the weighted average acceleration from 60mph to the 1/4 mile trap speed
                
                /* //Old Method using weighted average of the acceleration from 60mph to trap speed.
                 
                avgAccel = ((1.0f + (quarterMileSpeed - SIXTY_MPH_TO_METERS_SEC) / SIXTY_MPH_TO_METERS_SEC) *
                            quarterMileAccel - zeroSixtyAvgAccel) / ((quarterMileSpeed - SIXTY_MPH_TO_METERS_SEC) /
                                                                     SIXTY_MPH_TO_METERS_SEC);
                 */
                
                avgAccel = (quarterMileSpeed - SIXTY_MPH_TO_METERS_SEC) / (quarterMileTime - zeroSixty);
                
                if (sign(avgAccel) == -1) {
                    NSLog(@" ");
                    NSLog(@"Error: 60mph+ Average Acceleration is negative!");
                    NSLog(@" ");
                    [self didFail];
                    return;
                }
                
            } else {
                avgAccel = quarterMileAvgAccel;
            }
            
            // Caclulate the max speed achieved on the straight optimized for time. (See derivation in notes using time independant distance equation)
            vMax = sqrt((entrySpeed * entrySpeed * avgAccel + avgDecel * exitSpeed * exitSpeed
                         + 2.0f * avgAccel * avgDecel * straightLength) / (avgAccel + avgDecel));
            
            if (entrySpeed < exitSpeed) {
                
                // Check to see if we can decelerate enough in the given distance, if not then find the time to cover
                // the distance under maximum deceleration and just use that.
                
                // Using quadratic formula (Distance equation)
                CGFloat decelTime1 = (-exitSpeed + sqrt(exitSpeed * exitSpeed - 4.0f * 0.5f * -avgDecel * -straightLength)) / (2.0f * 0.5f * -avgDecel);
                CGFloat decelTime2 = (-exitSpeed - sqrt(exitSpeed * exitSpeed - 4.0f * 0.5f * -avgDecel * -straightLength)) / (2.0f * 0.5f * -avgDecel);
                
                if (sign(decelTime1) == -1 && sign(decelTime2) == -1) {
                    NSLog(@" ");
                    NSLog(@"Error: Complex results for entrySpeed < exitSpeed!");
                    NSLog(@" ");
                    [self didFail];
                    return;
                }
                
                // Check if decelTime1 or 2 is negative, if not then pick the smallest time
                if (sign(decelTime1) == -1) {
                    decelTime = decelTime2;
                } else if (sign(decelTime2) == -1) {
                    decelTime = decelTime1;
                } else {
                    // Both positive, choose the shortest time (when first reaching the desired point)
                    // Reject the larger number since that is the time is would take to slow to 0m/s and then return
                    if (decelTime1 < decelTime2) {
                        decelTime = decelTime1;
                    } else {
                        decelTime = decelTime2;
                    }
                }
                
                CGFloat speed = -avgDecel * decelTime + exitSpeed;
                
                if (speed > entrySpeed) {
                    accelTime = 0.0f;
                    simpleCase = YES;
                    segmentTopSpeed = exitSpeed;
                    vMax = exitSpeed;
                    //NSLog(@"Simple Case: entrySpeed < exitSpeed!");
                }
                
            } else if (entrySpeed > exitSpeed) {
                
                // Check to see if we can accelerate enough in the given distance, if not then find the time to cover
                // the distance under maximum acceleration and just use that.
                
                // Using quadratic formula (Distance equation)
                CGFloat accelTime1 = (-exitSpeed + sqrt(exitSpeed * exitSpeed - 4.0f * 0.5f * avgAccel * -straightLength)) / (2.0f * 0.5f * avgAccel);
                CGFloat accelTime2 = (-exitSpeed - sqrt(exitSpeed * exitSpeed - 4.0f * 0.5f * avgAccel * -straightLength)) / (2.0f * 0.5f * avgAccel);
                
                if (sign(accelTime1) == -1 && sign(accelTime2) == -1) {
                    NSLog(@" ");
                    NSLog(@"Error: Complex results for entrySpeed > exitSpeed!");
                    NSLog(@" ");
                    [self didFail];
                    return;
                }
                
                // Check if accelTime1 or 2 is negative, if not then pick the smallest time
                if (sign(accelTime1) == -1) {
                    accelTime = accelTime2;
                } else if (sign(accelTime2) == -1) {
                    accelTime = accelTime1;
                } else {
                    if (accelTime1 < accelTime2) {
                        accelTime = accelTime1;
                    } else {
                        accelTime = accelTime2;
                    }
                }
                
                CGFloat speed = avgAccel * accelTime + exitSpeed;
                
                if (speed < entrySpeed) {
                    decelTime = 0.0f;
                    simpleCase = YES;
                    segmentTopSpeed = speed;
                    vMax = speed;
                    //NSLog(@"Simple Case: entrySpeed > exitSpeed!");
                }
            }
            
            if (!simpleCase) {
                
                if (vMax > maxSpeed) {
                    
                    accelTime = (maxSpeed - exitSpeed) / avgAccel;
                    decelTime = (entrySpeed - maxSpeed) / -avgDecel;
                    CGFloat accelDecelDistance = 0.5f * avgAccel * accelTime * accelTime + exitSpeed * accelTime
                    + 0.5f * -avgDecel * decelTime * decelTime + vMax * decelTime;
                    CGFloat cruiseDistance = straightLength - accelDecelDistance;
                    cruiseTime = cruiseDistance / maxSpeed;
                    segmentTopSpeed = maxSpeed;
                
                } else {
                    accelTime = (vMax - exitSpeed) / avgAccel;
                    decelTime = (entrySpeed - vMax) / -avgDecel;
                    segmentTopSpeed = vMax;
                }
            }
            
            segmentTopSpeed = segmentTopSpeed / METERS_SEC_PER_MPH;
            straightLength = straightLength * FEET_PER_METER;
            
            _trackTime += accelTime + decelTime + cruiseTime;
            _trackLength += straightLength;
            
            if (segmentTopSpeed > _topSpeed) {
                _topSpeed = segmentTopSpeed;
            }
            
            //NSLog(@"Straight Time Accelerating:%f s", accelTime);
            //NSLog(@"Straight Time Cruising:%f s", cruiseTime);
            //NSLog(@"Straight Time Decelerating:%f s", decelTime);
            //NSLog(@"Straight Length:%f ft", straightLength);
            //NSLog(@"Straight Top Speed:%f mph", segmentTopSpeed);
            
            CGFloat distance = 0.5f * avgAccel * accelTime * accelTime + exitSpeed * accelTime
            + 0.5f * -avgDecel * decelTime * decelTime + vMax * decelTime + cruiseTime * maxSpeed;
            
            distance = distance * FEET_PER_METER;
            
            if (!(fabs(distance - straightLength) < 1.0f)) {
                NSLog(@" ");
                NSLog(@"Error: Distance Check failed! Given:%f Computed:%f", straightLength, distance);
                NSLog(@" ");
                [self didFail];
                return;
            }
            
            //NSLog(@" ");
        }
    }
    
    if (!([_trackName rangeOfString:@"Mosport"].location == NSNotFound)) {
        fudgeFactor = -19.47f;
        ratio = _trackTime / 96.937180f * M_PI_2;
        //NSLog(@"Track Time (unadjusted):%f s", _trackTime);
    } else if (!([_trackName rangeOfString:@"Laguna"].location == NSNotFound)) {
        fudgeFactor = -17.19f;
        ratio = _trackTime / 117.459465f * M_PI_2;
        //NSLog(@"Track Time (unadjusted):%f s", _trackTime);
    } else if (!([_trackName rangeOfString:@"Monza"].location == NSNotFound)) {
        fudgeFactor = -2.0f;
        ratio = _trackTime / 131.472595f * M_PI_2;
        //NSLog(@"Track Time (unadjusted):%f s", _trackTime);
    } else if (!([_trackName rangeOfString:@"Monaco"].location == NSNotFound)) {
        fudgeFactor = -15.0f;
        ratio = _trackTime / 120.593338f * M_PI_2;
        //NSLog(@"Track Time (unadjusted):%f s", _trackTime);
    } else if (!([_trackName rangeOfString:@"Suzuka"].location == NSNotFound)) {
        fudgeFactor = 3.0f;
        ratio = _trackTime / 148.089828f * M_PI_2;
        //NSLog(@"Track Time (unadjusted):%f s", _trackTime);
    
    } else {
        fudgeFactor = -10.0f;
        ratio = _trackTime / 120.0f * M_PI_2;
    }
    
    if (ratio > M_PI_2) {
        ratio = M_PI_2;
    }
    
    // Keep result always positive, reduces fudgefactor as trackTime decreases.
    _trackTime += sin(ratio) * fudgeFactor;
    
    // Sleep so the app appears to be crunching numbers!
    sleep(_trackLength / 1500.0f);
    
    int min = (int)_trackTime / 60;
    int sec = (int)_trackTime % 60;
    NSString *minutes;
    NSString *seconds;
    
    if (min < 10) {
        minutes = [NSString stringWithFormat:@"0%d", min];
    } else {
        minutes = [NSString stringWithFormat:@"%d", min];
    }
    
    if (sec < 10) {
        seconds = [NSString stringWithFormat:@"0%d", sec];
    } else {
        seconds = [NSString stringWithFormat:@"%d", sec];
    }
    
    _trackTimeString = [NSString stringWithFormat:@"%@:%@", minutes, seconds];
    
    NSLog(@"Track Time:%@", _trackTimeString);
    NSLog(@"Calculated Track Length:%f ft", _trackLength);
    NSLog(@"Top Speed:%f mph", _topSpeed);
    NSLog(@" ");
    
    [self performSelectorOnMainThread:@selector(didFinishCalculating)
                                    withObject:self
                                 waitUntilDone:NO];
}

- (void)displayResults;
{
    self.trackTime_textField.text = [NSString stringWithFormat:@"%@", _trackTimeString];
    self.trackLength_textField.text = [NSString stringWithFormat:@"%.1f", _trackLength];
    self.topSpeed_textField.text = [NSString stringWithFormat:@"%.1f", _topSpeed];
    
    //[self.shareButton setImage: [[UIImage imageNamed: @"share-icon-64x64"] imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate] forState: UIControlStateNormal];
    self.shareButton.hidden = NO;
}

- (IBAction)share:(id)sender;
{
    
    self.shareButton.userInteractionEnabled = NO;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        //self.shareButton.highlighted = YES;
        self.shareButton.enabled = NO;
    }];
     
    
    NSString *track;
    
    if (!([_trackName rangeOfString:@"Mosport"].location == NSNotFound)) {
        track = @"Mosport";
    } else if (!([_trackName rangeOfString:@"Laguna Seca"].location == NSNotFound)) {
        track = @"Laguna Seca";
    } else if (!([_trackName rangeOfString:@"Monza"].location == NSNotFound)) {
        track = @"Monza";
    } else if (!([_trackName rangeOfString:@"Monaco"].location == NSNotFound)) {
        track = @"Monaco";
    } else if (!([_trackName rangeOfString:@"Suzuka"].location == NSNotFound)) {
        track = @"Suzuka";
    } else {
        track = _trackName;
    }

    NSString *text = [NSString stringWithFormat:@"I lapped %@ in %@ in a %@. Simulate your ride's #LapTime",
                      track, _trackTimeString, _carName];
    NSURL *url = [NSURL URLWithString:@"http://appstore.com/laptime"];
    //UIImage *image = [UIImage imageNamed:@"AppIcon60x60@2x"];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:@[text, url]
                                                                             applicationActivities:nil];
    
    [self presentViewController:controller animated:YES completion:nil];
    
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self
                                   selector:@selector(didFinishSharing) userInfo:nil repeats:NO];

}

- (void)didFinishSharing;
{
    self.shareButton.userInteractionEnabled = YES;
    //self.shareButton.highlighted = NO;
    self.shareButton.enabled = YES;
}

#pragma mark -
#pragma mark Simulate Delegate Methods

- (void)didFinishCalculating;
{
    [self displayResults];
    [self.activity stopAnimating];
    self.simButton.userInteractionEnabled = YES;
    self.simButton.highlighted = NO;
}

- (void)didFail;
{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @"Error"
                          message: @"Failed to produce a valid simulation with given vehicle specs."
                          delegate: self
                          cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    [self.activity stopAnimating];
    self.simButton.userInteractionEnabled = YES;
    self.simButton.highlighted = NO;
}

#pragma mark -

- (void)didReceiveMemoryWarning;
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Picker Delegate Methods

// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_tracks count];
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _tracks[row];
}

// Catpure the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSString *string = _tracks[row];
    self.trackName_textField.text = [NSString stringWithFormat:@"%@", string];
    [self loadTrackSegments:self.trackName_textField.text];
}

@end

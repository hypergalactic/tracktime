//
//  AppDelegate.h
//  TrackTime
//
//  Created by Robert Wasmann on 2015-04-03.
//  Copyright (c) 2015 Big Muddy Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


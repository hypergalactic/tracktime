//
//  MathFunctions.m
//  TrackTime
//
//  Created by Robert Wasmann on 2015-04-03.
//  Copyright (c) 2015 Big Muddy Games. All rights reserved.
//

#import "MathFunctions.h"

float sign(float n) { return (n < 0) ? -1 : +1; }

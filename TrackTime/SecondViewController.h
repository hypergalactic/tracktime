//
//  SecondViewController.h
//  TrackTime
//
//  Created by Robert Wasmann on 2015-04-03.
//  Copyright (c) 2015 Big Muddy Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SecondViewController;
@protocol SecondViewControllerDelegate <NSObject>
-(void)textFieldDidChange:(SecondViewController *)controller data:(float)textFieldValue;
@end


@interface SecondViewController : UIViewController 

@property (nonatomic)  UITextField *currentTextfieldId;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) id<SecondViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *name_textField;
@property (weak, nonatomic) IBOutlet UITextField *zeroSixty_textField;
@property (weak, nonatomic) IBOutlet UITextField *quarterMileTime_textField;
@property (weak, nonatomic) IBOutlet UITextField *quarterMileSpeed_textField;
@property (weak, nonatomic) IBOutlet UITextField *maxLateral_textField;
@property (weak, nonatomic) IBOutlet UITextField *braking_textField;
@property (weak, nonatomic) IBOutlet UITextField *topSpeed_textField;

- (IBAction)keyboardCloseClicked:(id)sender;

@end


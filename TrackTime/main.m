//
//  main.m
//  TrackTime
//
//  Created by Robert Wasmann on 2015-04-03.
//  Copyright (c) 2015 Big Muddy Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

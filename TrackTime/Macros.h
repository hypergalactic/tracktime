// Panzer Joust

#import <UIKit/UIKit.h>
#import <math.h>

#import <Foundation/Foundation.h>
#import <Availability.h>
#import "CGPointExtension.h"

/**
 @file
 cocos2d helper macros
 */

#ifdef __IPHONE_OS_VERSION_MAX_ALLOWED
#define __CC_PLATFORM_IOS 1
#elif defined(__MAC_OS_X_VERSION_MAX_ALLOWED)
#define __CC_PLATFORM_MAC 1
#endif

/** @def CC_SWAP
simple macro that swaps 2 variables
*/
#define CC_SWAP( x, y )			\
({ __typeof__(x) temp  = (x);		\
		x = y; y = temp;		\
})

/** @def CCRANDOM_MINUS1_1
 Returns a random float between -1 and 1.
 */
static inline CGFloat CCRANDOM_MINUS1_1(){ return (random() / (CGFloat)0x3fffffff ) - 1.0f; }

/** @def CCRANDOM_0_1
 Returns a random float between 0 and 1.
 */
static inline CGFloat CCRANDOM_0_1(){ return random() / (CGFloat)0x7fffffff;}

/** @def CCRANDOM_IN_UNIT_CIRCLE
 Returns a random CGPoint with a length less than 1.0.
 */
static inline CGPoint CCRANDOM_IN_UNIT_CIRCLE()
{
	while(TRUE){
		CGPoint p = ccp(CCRANDOM_MINUS1_1(), CCRANDOM_MINUS1_1());
		if(ccpLengthSQ(p) < 1.0) return p;
	}
}

/** @def CCRANDOM_ON_UNIT_CIRCLE
 Returns a random CGPoint with a length equal to 1.0.
 */
static inline CGPoint CCRANDOM_ON_UNIT_CIRCLE()
{
	while(TRUE){
		CGPoint p = ccp(CCRANDOM_MINUS1_1(), CCRANDOM_MINUS1_1());
		CGFloat lsq = ccpLengthSQ(p);
		if(0.1 < lsq && lsq < 1.0f) return ccpMult(p, 1.0/sqrt(lsq));
	}
}

/** @def CC_DEGREES_TO_RADIANS
 converts degrees to radians
 */
#define CC_DEGREES_TO_RADIANS(__ANGLE__) ((__ANGLE__) * 0.01745329252f) // PI / 180

/** @def CC_RADIANS_TO_DEGREES
 converts radians to degrees
 */
#define CC_RADIANS_TO_DEGREES(__ANGLE__) ((__ANGLE__) * 57.29577951f) // PI * 180

// Util functions for rescaling CGRects and CGSize, use ccpMult() for CGPoints.

static inline CGRect CC_RECT_SCALE(CGRect rect, CGFloat scale){
	return CGRectMake(
		rect.origin.x * scale,
		rect.origin.y * scale,
		rect.size.width * scale,
		rect.size.height * scale
	);
}

static inline CGSize CC_SIZE_SCALE(CGSize size, CGFloat scale){
	return CGSizeMake(size.width * scale, size.height * scale);
}

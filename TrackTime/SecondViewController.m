//
//  SecondViewController.m
//  TrackTime
//
//  Created by Robert Wasmann on 2015-04-03.
//  Copyright (c) 2015 Big Muddy Games. All rights reserved.
//

#import "SecondViewController.h"
#import "FirstViewController.h"

#define STD_LATERAL_ACCEL 0.97f
#define kFieldSeperation 84  // Default distance between UI elements in *most* cases. Used for aligning the scroll view with keyboard.
#define MIN_0_60_TIME 1.0f
#define MAX_0_60_TIME 60.0f
#define MIN_QUARTERMILE_TIME 1.0f
#define MAX_QUARTERMILE_TIME 120.0f
#define MIN_QUARTMILE_SPEED 60.0f
#define MAX_QUARTMILE_SPEED 200.0f
#define MIN_LATERAL_ACCEL 0.1f
#define MAX_LATERAL_ACCEL 10.0f
#define MIN_BRAKING_DIST 10.0f
#define MAX_BRAKING_DIST 1000.0f
#define MIN_TOPSPEED 60.0f
#define MAX_TOPSPEED 1000.0f

@interface SecondViewController ()


@end

@implementation SecondViewController
{
    CGPoint         _scrollOffset;
    CGSize          _kbSize;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self getSettings];
    [self registerForKeyboardNotifications];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    id carName = [defaults objectForKey:@"defaultNameKEY"];
    
    if (carName == nil) {
        self.name_textField.text = [NSString stringWithFormat:@"2007 Porsche 911 Turbo"];
        self.zeroSixty_textField.text = [NSString stringWithFormat:@"%.1f", 3.3f];
        self.quarterMileTime_textField.text = [NSString stringWithFormat:@"%.1f", 11.6f];
        self.quarterMileSpeed_textField.text = [NSString stringWithFormat:@"%.1f", 118.3f];
        self.maxLateral_textField.text = [NSString stringWithFormat:@"%.2f", 0.97f];
        self.braking_textField.text = [NSString stringWithFormat:@"%.1f", 116.0f];
        self.topSpeed_textField.text = [NSString stringWithFormat:@"%.0f", 193.0f];
        
        [self saveSettings];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField != self.name_textField) {
        
        //self.trackName_textField.inputView = self.pickerView;
        
        UIToolbar *myToolbar = [[UIToolbar alloc] initWithFrame:
                                CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height / 17.0f)];
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                    target:self action:@selector(keyboardCloseClicked:)];
        
        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        //using default text field delegate method here, here you could call
        //myTextField.resignFirstResponder to dismiss the views
        [myToolbar setItems:[NSArray arrayWithObjects:flex, doneButton, nil] animated:NO];
        textField.inputAccessoryView = myToolbar;
    }

    _currentTextfieldId = textField;
    [self autoScrollView];
}

/*
- (void)inputAccessoryViewDidFinish;
{
    [self.currentTextfieldId resignFirstResponder];
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Keyboard

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
// Called AFTER textFieldDidBeginEditing
- (void)keyboardWillShow:(NSNotification*)aNotification
{
    
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the UIScrollView if the keyboard is already shown.
    // This can happen if the user, after editing a UITextField, scrolls the resized UIScrollView to another UITextField and attempts
    // to edit the next UITextField.  If we were to resize the UIScrollView again, it would be disastrous.
    // NOTE: The keyboard notification will fire even when the keyboard is already shown.
    //if (_keyboardIsShown) {
    //    return;
    //}
    
    NSDictionary* info = [aNotification userInfo];
    _kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    // Call autoScroll since the previous call in textFieldDidBeginEditing would have returned since the kbSize had not been set yet. 
    [self autoScrollView];
    //_keyboardIsShown = YES;
    
}

-(void)autoScrollView;
{
    if (_kbSize.width < 1.0f && _kbSize.height < 1.0f) {
        return;
    }
    
    // Height is always taken as the direction along the longer side of the ipad. Find the shortest dimension to use as height.
    double kbHeight = _kbSize.height;
    if (kbHeight > _kbSize.width) {
        kbHeight = _kbSize.width;
    }
    
    double textYloc = [self.currentTextfieldId convertPoint:CGPointMake(0, 0) toView:self.view].y;

    if (textYloc - _scrollView.contentOffset.y + self.currentTextfieldId.frame.size.height + kFieldSeperation > self.view.frame.size.height - kbHeight) {
        CGPoint scrollPoint = CGPointMake(0, textYloc - self.view.frame.size.height + kbHeight + self.currentTextfieldId.frame.size.height + kFieldSeperation);
        _scrollOffset = _scrollView.contentOffset;
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    //_keyboardIsShown = NO;
    
}


#pragma mark -
#pragma mark IBACTION EVENT METHODS

- (IBAction)keyboardCloseClicked:(id)sender;
{
    //--- EDIT CHECKS
    if ( ![self editChecks])
        return;
    
    //--- HIDE KEYBOARD
    UITextField *currentTextfield = (UITextField *)_currentTextfieldId;
    [currentTextfield resignFirstResponder];
    
    [self saveSettings];
    
    // Update First View Controller
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    FirstViewController *fvc = [self.tabBarController.viewControllers objectAtIndex:0];
    fvc.carName_textField.text = [defaults objectForKey:@"defaultNameKEY"];
}

#pragma mark -
#pragma mark ALERTS METHODS

- (void) displayAlert: (NSString *)pFieldName withMessage:(NSString *)pMsg
{
    NSString *title = [NSString stringWithFormat:@"Invalid %@",pFieldName];
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: title
                          message: pMsg
                          delegate: self
                          cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

#pragma mark -
#pragma mark EDIT CHECKS METHODS

- (BOOL) editChecks;
{
    if ([self.zeroSixty_textField.text doubleValue] <= MIN_0_60_TIME) {
        NSString *fieldName = @"0-60mph Time";
        NSString *msg = [NSString stringWithFormat:@"0-60 time must be greater than %.1f sec", MIN_0_60_TIME];
        [self displayAlert:fieldName withMessage:msg];
        [self.zeroSixty_textField becomeFirstResponder];
        return NO;
    }
    if ([self.zeroSixty_textField.text doubleValue] >= MAX_0_60_TIME) {
        NSString *fieldName = @"0-60mph Time";
        NSString *msg = [NSString stringWithFormat:@"0-60 time must be less than %.1f sec", MAX_0_60_TIME];
        [self displayAlert:fieldName withMessage:msg];
        [self.zeroSixty_textField becomeFirstResponder];
        return NO;
    }
    if ([self.quarterMileTime_textField.text doubleValue] <= [self.zeroSixty_textField.text doubleValue]) {
        NSString *fieldName = @"1/4 Mile Time";
        NSString *msg = @"1/4 mile time must be greater than 0-60mph time.";
        [self displayAlert:fieldName withMessage:msg];
        [self.quarterMileTime_textField becomeFirstResponder];
        return NO;
    }
    if ([self.quarterMileTime_textField.text doubleValue] <= MIN_QUARTERMILE_TIME) {
        NSString *fieldName = @"1/4 Mile Time";
        NSString *msg = [NSString stringWithFormat:@"1/4 mile time must be greater than %.1f sec", MIN_QUARTERMILE_TIME];
        [self displayAlert:fieldName withMessage:msg];
        [self.quarterMileTime_textField becomeFirstResponder];
        return NO;
    }
    if ([self.quarterMileTime_textField.text doubleValue] >= MAX_QUARTERMILE_TIME) {
        NSString *fieldName = @"1/4 Mile Time";
        NSString *msg = [NSString stringWithFormat:@"1/4 mile time must be less than %.1f sec", MAX_QUARTERMILE_TIME];
        [self displayAlert:fieldName withMessage:msg];
        [self.quarterMileTime_textField becomeFirstResponder];
        return NO;
    }
    if ([self.quarterMileSpeed_textField.text doubleValue] <= MIN_QUARTMILE_SPEED) {
        NSString *fieldName = @"1/4 Mile Trap Speed";
        NSString *msg = [NSString stringWithFormat:@"1/4 mile trap speed must be greater than %.1f mph", MIN_QUARTMILE_SPEED];
        [self displayAlert:fieldName withMessage:msg];
        [self.quarterMileSpeed_textField becomeFirstResponder];
        return NO;
    }
    if ([self.quarterMileSpeed_textField.text doubleValue] >= MAX_QUARTMILE_SPEED) {
        NSString *fieldName = @"1/4 Mile Trap Speed";
        NSString *msg = [NSString stringWithFormat:@"1/4 mile trap speed must be less than %.1f mph", MAX_QUARTMILE_SPEED];
        [self displayAlert:fieldName withMessage:msg];
        [self.quarterMileSpeed_textField becomeFirstResponder];
        return NO;
    }
    if ([self.maxLateral_textField.text doubleValue] <= MIN_LATERAL_ACCEL) {
        NSString *fieldName = @"Lateral Acceleration";
        NSString *msg = [NSString stringWithFormat:@"Lateral acceleration must be greater than %.1f G", MIN_LATERAL_ACCEL];
        [self displayAlert:fieldName withMessage:msg];
        [self.maxLateral_textField becomeFirstResponder];
        return NO;
    }
    if ([self.maxLateral_textField.text doubleValue] >= MAX_LATERAL_ACCEL) {
        NSString *fieldName = @"Lateral Acceleration";
        NSString *msg = [NSString stringWithFormat:@"Lateral acceleration must be less than %.1f G", MAX_LATERAL_ACCEL];
        [self displayAlert:fieldName withMessage:msg];
        [self.maxLateral_textField becomeFirstResponder];
        return NO;
    }
    if ([self.braking_textField.text doubleValue] <= MIN_BRAKING_DIST) {
        NSString *fieldName = @"Braking Distance 60-0";
        NSString *msg = [NSString stringWithFormat:@"Braking distance must be greater than %.1f ft", MIN_BRAKING_DIST];
        [self displayAlert:fieldName withMessage:msg];
        [self.braking_textField becomeFirstResponder];
        return NO;
    }
    if ([self.braking_textField.text doubleValue] >= MAX_BRAKING_DIST) {
        NSString *fieldName = @"Braking Distance 60-0";
        NSString *msg = [NSString stringWithFormat:@"Braking distance must be less than %.1f ft", MAX_BRAKING_DIST];
        [self displayAlert:fieldName withMessage:msg];
        [self.braking_textField becomeFirstResponder];
        return NO;
    }
    if ([self.topSpeed_textField.text doubleValue] <= MIN_TOPSPEED) {
        NSString *fieldName = @"Top Speed";
        NSString *msg = [NSString stringWithFormat:@"Top speed must be greater than %.1f mph", MIN_TOPSPEED];
        [self displayAlert:fieldName withMessage:msg];
        [self.topSpeed_textField becomeFirstResponder];
        return NO;
    }
    if ([self.topSpeed_textField.text doubleValue] >= MAX_TOPSPEED) {
        NSString *fieldName = @"Top Speed";
        NSString *msg = [NSString stringWithFormat:@"Top speed must be less than %.1f mph", MAX_TOPSPEED];
        [self displayAlert:fieldName withMessage:msg];
        [self.topSpeed_textField becomeFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)saveSettings;
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.name_textField.text forKey:@"defaultNameKEY"];
    [defaults setDouble:[self.zeroSixty_textField.text floatValue] forKey:@"defaultZeroSixtyKEY"];
    [defaults setDouble:[self.quarterMileTime_textField.text floatValue] forKey:@"defaultQuarterMileTimeKEY"];
    [defaults setDouble:[self.quarterMileSpeed_textField.text floatValue] forKey:@"defaultQuarterMileSpeedKEY"];
    [defaults setDouble:[self.maxLateral_textField.text floatValue] forKey:@"defaultMaxLateralKEY"];
    [defaults setDouble:[self.braking_textField.text floatValue] forKey:@"defaultBrakingKEY"];
    [defaults setDouble:[self.topSpeed_textField.text floatValue] forKey:@"defaultTopSpeedKEY"];
}

- (void)getSettings;
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.name_textField.text = [defaults objectForKey:@"defaultNameKEY"];
    
    double value = (double)[defaults doubleForKey:@"defaultZeroSixtyKEY"];
    self.zeroSixty_textField.text = [NSString stringWithFormat:@"%.1f", value];
    
    value = (double)[defaults doubleForKey:@"defaultQuarterMileTimeKEY"];
    self.quarterMileTime_textField.text = [NSString stringWithFormat:@"%.1f", value];
    
    value = (double)[defaults doubleForKey:@"defaultQuarterMileSpeedKEY"];
    self.quarterMileSpeed_textField.text = [NSString stringWithFormat:@"%.1f", value];
    
    value = (double)[defaults doubleForKey:@"defaultMaxLateralKEY"];
    self.maxLateral_textField.text = [NSString stringWithFormat:@"%.2f", value];
    
    value = (double)[defaults doubleForKey:@"defaultBrakingKEY"];
    self.braking_textField.text = [NSString stringWithFormat:@"%.1f", value];
    
    value = (double)[defaults doubleForKey:@"defaultTopSpeedKEY"];
    self.topSpeed_textField.text = [NSString stringWithFormat:@"%.0f", value];
    
    [defaults synchronize];
}

@end
